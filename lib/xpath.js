'use strict'

const xpath = require('xpath')
const Dom = require('xmldom').DOMParser
const reportSupport = require('@djencks/asciidoctor-report-support')

const XPATH_INCLUDE = 'xpath$'
const XPATH_COUNT_INCLUDE = 'xpathcount$'
const XPATH_UNIQUE_COUNT_INCLUDE = 'indexuniquecount$'

let pipelineConfig = { playbookDir: undefined, debug: false, trace: false }

module.exports.register = function (registry, config) {
  const pc = reportSupport.pipelineConfigure(registry, config, module.exports, doQuery({}), '@djencks/asciidoctor-xpath', undefined, { xml: asDom })
  if (pc) {
    //called as Antora extension register.
    pipelineConfig = pc
    return
  }

  //called as Asciidoctor extension register.

  // For a per-page extension in Antora, config will have the structure:
  //{ file, // the vfs file being processed
  // contentCatalog, // the Antora content catalog
  // config // the asciidoc section of the playbook, enhanced with asciidoc attributes from the component descriptor.
  // }

  // For "required" global extensions in Antora, config will be undefined (replaced with {}).
  // Other loading code may use other config options.

  //File access

  const vfs = (config.vfs && typeof config.vfs.read === 'function')
    ? config.vfs
    //Antora support
    : (config.file && config.contentCatalog && typeof config.contentCatalog.resolveResource === 'function')
      ? {
        read: (resourceId) => {
          const target = config.contentCatalog.resolveResource(resourceId, config.file.src)
          if (target) return target.contents
          return undefined
        },
      }
      //look in the file system
      : fsAccess()

  function fsAccess () {
    const fs = require('fs')
    return {
      read: (path, encoding = 'utf8') => {
        if (path.startsWith('file://')) {
          return fs.readFileSync(path.substr('file://'.length), encoding)
        }
        return fs.readFileSync(path, encoding)
      },
    }
  }

  function asDom (file, logContext) {
    return toDom(file.path, file.contents, logContext)
  }

  function toDom (target, contents, logContext) {
    try {
      return new Dom().parseFromString(contents.toString())
    } catch (error) {
      logContext.logger.warn({ msg: `Invalid source file ${target}`, error, ...logContext.use })
    }
  }

  function doQuery (cache, vfs = null) {
    return function (target, { query, namespaces }, fn, contentCatalog, file, logContext,
      contentExtractor = null, transform = (items) => items) {
      logContext.debug && logContext.logger.debug({ query, ...logContext.use })
      let data = cache[target]
      if (!data) {
        let found
        const contents = contentCatalog
          ? ((found = contentCatalog.resolveResource(target, file.src)) && found.contents)
          : vfs.read(target)
        if (contents) {
          data = toDom(target, contents, logContext)
          if (!data) {
            return
          }
          cache[target] = data
        } else {
          logContext.logger.warn({ msg: `Target ${target} not found`, ...logContext.use })
          return
        }
      }
      try {
        const namespaceMap = (namespaces || '').split(',').reduce((accum, namespace) => {
          const { name, expr } = reportSupport.splitOnce(namespace, '=')
          accum[name] = expr
          return accum
        }, {})
        const select = xpath.useNamespaces(namespaceMap)
        let result = select(query, data)
        //make the results more javascript like
        result = result.map((element) => {
          const contents = {}
          for (let i = 0; i < element.attributes.length; i++) {
            const attribute = element.attributes.item(i)
            contents[attribute.name] = attribute.value
          }
          for (let i = 0; i < element.childNodes.length; i++) {
            const node = element.childNodes[i]
            if (node.nodeType === node.ELEMENT_NODE) contents[node.nodeName] = node.textContent
          }
          return contents
        })
        transform = transform.bind({ data, namespaceMap, select })
        //should transform happen before the map to js? most likely it's for sorting, so probably not.
        result = transform(result)
        result.forEach(fn)
      } catch (e) {
        logContext.logger.warn({ msg: `Processing error for formatting or query ${query}, error: ${e.msg || e}`, ...logContext.use })
      }
    }
  }

  function doRegister (registry) {
    const reports = reportSupport('xpath', doQuery({}, vfs), pipelineConfig, registry, config)
    if (typeof registry.block === 'function') {
      registry.block(reports.templateBlockProcessor('xpathBlock', reportSupport.formatFunction, reports.processBlocks))
      registry.block(reports.templateBlockProcessor('xpathTemplate', reportSupport.formatFunction, reports.processTemplates))
    } else {
      reports.logger.warn('No \'block\' method on alleged registry')
    }
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(reports.tableProcessor('xpathTable', '|', reportSupport.formatFunction))
      registry.blockMacro(reports.descriptionListProcessor('xpathDescriptionList', reportSupport.formatFunction))
      registry.blockMacro(reports.listProcessor('xpathList', 'ulist', '*', reportSupport.formatFunction))
      registry.blockMacro(reports.listProcessor('xpathOrderedList', 'olist', '.', reportSupport.formatFunction))
      registry.blockMacro(reports.expressionProcessor('xpathExpression', reportSupport.formatFunction, reports.evaluateExpressionBlock))
    } else {
      reports.logger.warn('No \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.inlineMacro === 'function') {
      registry.inlineMacro(reports.inlineCountProcessor('xpathCount'))
      registry.inlineMacro(reports.inlineUniqueCountProcessor('xpathUniqueCount', reportSupport.formatFunction))
      registry.inlineMacro(reports.expressionProcessor('xpathExpression', reportSupport.formatFunction, reports.evaluateExpressionInline))
    } else {
      reports.logger.warn('No \'inlineMacro\' method on alleged registry')
    }
    if (typeof registry.includeProcessor === 'function') {
      registry.prefer('include_processor', reports.attributesIncludeProcessor(XPATH_INCLUDE, reportSupport.formatFunction, true))
      registry.prefer('include_processor', reports.countAttributesIncludeProcessor(XPATH_COUNT_INCLUDE))
      registry.prefer('include_processor', reports.uniqueCountAttributesIncludeProcessor(XPATH_UNIQUE_COUNT_INCLUDE, reportSupport.formatFunction, true))
    } else {
      reports.logger.warn('No \'includeProcessor\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}
