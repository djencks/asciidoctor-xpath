# Asciidoctor xpath Extension
:version: 0.1.3-rc.2

`@djencks/asciidoctor-xpath` provides an Asciidoctor.js extension to add the results of an xpath query on an xml document to an asciidoc document.

WARNING:: This is based on and link:https://github.com/browserify/static-eval[static-eval].
This should not be allowed to execute arbitrary user-supplied code.
For this reason, THIS EXTENSION SHOULD ONLY BE USED ON KNOWN AND TRUSTED CONTENT!.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-xpath/-/blob/master/README.adoc.

## Installation

Available through npm as @djencks/asciidoctor-xpath.

The project git repository is https://gitlab.com/djencks/asciidoctor-xpath

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-xpath/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/djencks/asciidoctor-xpath/-/blob/master/README.adoc

## Antora Example project

An example project showing some uses of this extension may appear soon at extensions/xpath-extension in `https://gitlab.com/djencks/simple-examples`.
