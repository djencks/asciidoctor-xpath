/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const xpath = require('./../lib/xpath')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

describe('xpath tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
    const loggerManager = asciidoctor.LoggerManager
    const defaultLogger = loggerManager.getLogger()
    const jsonFormatter = asciidoctor.LoggerManager.newFormatter('JsonFormatter', {
      call: function (severity, time, programName, message) {
        // const text = message['text']
        // const sourceLocation = message['source_location']
        return JSON.stringify({
          programName: programName,
          time,
          message,
          severity,
        }) + '\n'
      },
    })
    defaultLogger.setFormatter(jsonFormatter)
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const vfss = [
      {
        vfsType: 'plain',
        config: {
          vfs: {
            read: (filespec) => {
              filespec = filespec.substring(filespec.lastIndexOf(':') + 1).substring(filespec.lastIndexOf('$') + 1)
              return seed.filter((f) => filespec === f.relative).reduce((accum, f) => {
                accum.contents = Buffer.from(f.contents)
                return accum
              }, {}).contents
            },

          },
        },
      },
      {
        vfsType: 'antora',
        config: {
          file: {
            src: {
              version: '4.5',
              component: 'component-a',
              module: 'module-a',
              family: 'page',
            },
          },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
    ]
    return vfss
  }

  ;[{
    type: 'global',
    f: (text, config) => {
      xpath.register(asciidoctor.Extensions, config)
      return asciidoctor.load(text)
    },
  },
  {
    type: 'registry',
    f: (text, config) => {
      const registry = xpath.register(asciidoctor.Extensions.create(), config)
      return asciidoctor.load(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    prepareVfss([
      {
        version: '4.5',
        family: 'page',
        relative: 'page-a.adoc',
        contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
      },
      {
        version: '4.5',
        family: 'example',
        relative: 'data1.xml',
        contents: `<?xml version="1.0" encoding="UTF-8"?>
<data>
  <row name="foo">
    <description>fooish</description>
    <uniqueish>1</uniqueish>
  </row>
  <row name="bar">
    <description>barish</description>
    <uniqueish>2</uniqueish>
  </row>
  ],
  <objects>
    <object id="field1" p1="a" p2="b" uniqueish="1"/>
    <object id="field2" p1="c" p2="d" uniqueish="1"/>
    <object id="field3" p1="e" p2="f" uniqueish="2"/>
  </objects>
</data>`,
      },
      //Content presumably AL2 licensed, from Apache Camel.
      {
        version: '4.5',
        family: 'example',
        relative: 'data2.xml',
        contents: `<?xml version="1.0" encoding="UTF-8"?>
<!--

    Licensed to the Apache Software Foundation (ASF) under one or more
    contributor license agreements.  See the NOTICE file distributed with
    this work for additional information regarding copyright ownership.
    The ASF licenses this file to You under the Apache License, Version 2.0
    (the "License"); you may not use this file except in compliance with
    the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.apache.camel.quarkus</groupId>
        <artifactId>camel-quarkus-activemq-parent</artifactId>
        <version>2.0.1-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <artifactId>camel-quarkus-activemq</artifactId>
    <name>Camel Quarkus :: ActiveMQ :: Runtime</name>

    <properties>
        <camel.quarkus.jvmSince>1.0.0</camel.quarkus.jvmSince>
        <camel.quarkus.nativeSince>1.0.0</camel.quarkus.nativeSince>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.apache.camel.quarkus</groupId>
                <artifactId>camel-quarkus-bom</artifactId>
                <version>$\{project.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.apache.camel.quarkus</groupId>
            <artifactId>camel-quarkus-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.camel.quarkus</groupId>
            <artifactId>camel-quarkus-jms</artifactId>
            <!-- ActiveMQ is bound to JMS 1.1 -->
            <exclusions>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-jms_2.0_spec</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-activemq</artifactId>
            <!-- remove broker as it brings some classes that brak native compilation -->
            <!-- we should investigate how to substitue JMX on ActiveMQ -->
            <exclusions>
                <exclusion>
                    <groupId>org.apache.activemq</groupId>
                    <artifactId>activemq-broker</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.graalvm.nativeimage</groupId>
            <artifactId>svm</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>io.quarkus</groupId>
                <artifactId>quarkus-bootstrap-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>io.quarkus</groupId>
                            <artifactId>quarkus-extension-processor</artifactId>
                            <version>$\{quarkus.version}</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>full</id>
            <activation>
                <property>
                    <name>!quickly</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.camel.quarkus</groupId>
                        <artifactId>camel-quarkus-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>update-extension-doc-page</id>
                                <goals>
                                    <goal>update-extension-doc-page</goal>
                                </goals>
                                <phase>process-classes</phase>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
`,
      },
    ]).forEach(({ vfsType, config }) => {
      ;['', 'query='].forEach((query) => {
        ;[['', '`', '|']].forEach(([suffix, delimiter, cellDelimiter]) => {
          ;['', 'cellformats='].forEach((cellformats) => {
            describe('xpathTableMacro tests', () => {
              ;['data1.xml'].forEach((dataFile) => {
                it(`simple table test, ${type}, ${vfsType}, dataFile: ${dataFile}, suffix: ${suffix}`, () => {
                  const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

xpathTable${suffix}::example$${dataFile}[${query}'//row', ${cellformats}'name${cellDelimiter}description']

`, config)
                  const html = doc.convert()
                  expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">foo</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">bar</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">barish</p></td>
</tr>
</tbody>
</table>`)
                })

                it(`templated table test, ${type}, ${vfsType}, dataFile: ${dataFile}, suffix: ${suffix}`, () => {
                  const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

xpathTable${suffix}::example$${dataFile}[${query}'//row', ${cellformats}'${delimiter}the \${name} described by${delimiter}${cellDelimiter}${delimiter}the description \${description}${delimiter}']

`, config)
                  const html = doc.convert()
                  expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the foo described by</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the bar described by</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description barish</p></td>
</tr>
</tbody>
</table>`)
                })

                it(`templated table test with escaped commas, ${type}, ${vfsType}, dataFile: ${dataFile}, suffix: ${suffix}`, () => {
                  const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

xpathTable${suffix}::example$${dataFile}[${query}'//row', ${cellformats}'${delimiter}the \${name} described by\\, incidentally\\,${delimiter}${cellDelimiter}${delimiter}the description \${description}${delimiter}']

`, config)
                  const html = doc.convert()
                  expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the foo described by, incidentally,</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the bar described by, incidentally,</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description barish</p></td>
</tr>
</tbody>
</table>`)
                })

                it(`object table test, ${type}, ${vfsType}, dataFile: ${dataFile}, suffix: ${suffix}`, () => {
                  const doc = f(`

[cols='1,2,3',options="header"]
|===
|name
|p1
|p2
|===

xpathTable${suffix}::example$${dataFile}[${query}'/data/objects/object', ${cellformats}'${delimiter}\${id}${delimiter}${cellDelimiter}${delimiter}\${p1}${delimiter}${cellDelimiter}${delimiter}\${p2}${delimiter}']

`, config)
                  const html = doc.convert()
                  expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 16.6666%;">
<col style="width: 33.3333%;">
<col style="width: 50.0001%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">p1</th>
<th class="tableblock halign-left valign-top">p2</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field1</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">a</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">b</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field2</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">c</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">d</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field3</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">e</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">f</p></td>
</tr>
</tbody>
</table>`)
                })
              })
              it(`camel object table test, ${type}, ${vfsType}, suffix: ${suffix}, cellDelimiter: ${cellDelimiter}`, () => {
                const doc = f(`
:expr: ${cellformats}'${delimiter}*\${groupId}* (\${groupId})${delimiter}${cellDelimiter} \
${delimiter}\${artifactId}${delimiter}'

[width="100%",cols="5,5",options="header"]
|===
| GroupId | ArtifactId
|===

xpathTable${suffix}::example$data2.xml[${query}'/mvn:project/mvn:dependencies/mvn:dependency', {expr}, namespaces='mvn=http://maven.apache.org/POM/4.0.0']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 50%;">
<col style="width: 50%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">GroupId</th>
<th class="tableblock halign-left valign-top">ArtifactId</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>org.apache.camel.quarkus</strong> (org.apache.camel.quarkus)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">camel-quarkus-core</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>org.apache.camel.quarkus</strong> (org.apache.camel.quarkus)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">camel-quarkus-jms</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>org.apache.camel</strong> (org.apache.camel)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">camel-activemq</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>org.graalvm.nativeimage</strong> (org.graalvm.nativeimage)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">svm</p></td>
</tr>
</tbody>
</table>`)
              })
            })
          })
          ;['', 'format='].forEach((format) => {
            describe(`xpathListMacro tests, ${type}, ${vfsType}, suffix: ${suffix}`, () => {
              it('simple ulist array test', () => {
                const doc = f(`

xpathList${suffix}::example$data1.xml[${query}'//row', ${format}'${delimiter}\${name}: \${description}${delimiter}']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="ulist">
<ul>
<li>
<p>foo: fooish</p>
</li>
<li>
<p>bar: barish</p>
</li>
</ul>
</div>`)
              })

              it('simple ulist object test', () => {
                const doc = f(`

xpathList${suffix}::example$data1.xml[${query}'/data/objects/object', ${format}'${delimiter}\${id}: \${p1} and \${p2}${delimiter}']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="ulist">
<ul>
<li>
<p>field1: a and b</p>
</li>
<li>
<p>field2: c and d</p>
</li>
<li>
<p>field3: e and f</p>
</li>
</ul>
</div>`)
              })

              it('simple olist array test', () => {
                const doc = f(`

xpathOrderedList::example$data1.xml[${query}'//row', ${format}'\`\${name}: \${description}\`']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="olist">
<ol class="">
<li>
<p>foo: fooish</p>
</li>
<li>
<p>bar: barish</p>
</li>
</ol>
</div>`)
              })

              it('simple olist object test', () => {
                const doc = f(`

xpathOrderedList::example$data1.xml[${query}'/data/objects/object', ${format}'\`\${id}: \${p1} and \${p2}\`']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="olist">
<ol class="">
<li>
<p>field1: a and b</p>
</li>
<li>
<p>field2: c and d</p>
</li>
<li>
<p>field3: e and f</p>
</li>
</ol>
</div>`)
              })
            })
            describe(`xpathUniqueCountMacro tests, ${type}, ${vfsType}, suffix: ${suffix}, delimiter: ${delimiter}`, () => {
              it('simple unique count array test', () => {
                const doc = f(`

xpathUniqueCount:example$data1.xml[${query}'//row', ${format}'uniqueish']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
              })

              it('simple unique count object test', () => {
                const doc = f(`

xpathUniqueCount:example$data1.xml[${query}'/data/objects/object', ${format}"{backtick}\${uniqueish}{backtick}"]

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
              })
            })
            describe(`xpathExpressionMacro tests, ${type}, ${vfsType}, suffix: ${suffix}, delimiter: ${delimiter}`, () => {
              it('simple expression test', () => {
                const doc = f(`
:shortName: data2

//xpathExpression${suffix}:example$data2.xml[${query}'$.component', ${format}'${delimiter}\${syntax}${delimiter}']

[subs='+attributes,macros']
----
xpathExpression:example$\{shortName}.xml[${query}'/mvn:project', ${format}artifactId, namespaces='mvn=http://maven.apache.org/POM/4.0.0']
----

`, config)
                const html = doc.convert()
                expect(html).to.equal('<div class="listingblock">\n<div class="content">\n<pre>camel-quarkus-activemq</pre>\n</div>\n</div>')
              })
            })
          })
          ;['', 'subjectformat='].forEach((subjectformat) => {
            ;['', 'descriptionformat='].forEach((descriptionformat) => {
              describe('xpathDListMacro tests', () => {
                it('simple dlist array test', () => {
                  const doc = f(`

xpathDescriptionList::example$data1.xml[${query}'//row', ${subjectformat}'\`\${name}\`', ${descriptionformat}'\`\${description}\`']

`, config)
                  const html = doc.convert()
                  expect(html).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">foo</dt>
<dd>
<p>fooish</p>
</dd>
<dt class="hdlist1">bar</dt>
<dd>
<p>barish</p>
</dd>
</dl>
</div>`)
                })

                it('simple dlist object test', () => {
                  const doc = f(`

xpathDescriptionList::example$data1.xml[${query}'/data/objects/object', ${subjectformat}'\`\${id}\`', ${descriptionformat}'\`\${p1} and \${p2}\`']

`, config)
                  const html = doc.convert()
                  expect(html).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">field1</dt>
<dd>
<p>a and b</p>
</dd>
<dt class="hdlist1">field2</dt>
<dd>
<p>c and d</p>
</dd>
<dt class="hdlist1">field3</dt>
<dd>
<p>e and f</p>
</dd>
</dl>
</div>`)
                })
              })
            })
          })
        })
        describe('xpathCountMacro tests', () => {
          it('simple count array test', () => {
            const doc = f(`

xpathCount:example$data1.xml[${query}'//row']

`, config)
            const html = doc.convert()
            expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
          })

          it('simple count object test', () => {
            const doc = f(`

xpathCount:example$data1.xml[${query}'/data/objects/object']

`, config)
            const html = doc.convert()
            expect(html).to.equal(`<div class="paragraph">
<p>3</p>
</div>`)
          })
        })
        ;['', 'formats='].forEach((formats) => {
          ;['', 'target='].forEach((target) => {
            describe('xpathBlock tests', () => {
              it('simple block array test', () => {
                const doc = f(`

[xpathBlock, ${target}example$data1.xml, ${query}'//row', 'name,description,uniqueish']
----
== Section {name}

A description: {description} that is uniquely {uniqueish}
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect1">
<h2 id="_section_foo">Section foo</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A description: fooish that is uniquely 1</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_bar">Section bar</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A description: barish that is uniquely 2</p>
</div>
</div>
</div>`)
              })
              it('simple block object test', () => {
                const doc = f(`

[xpathBlock, ${target}example$data1.xml, ${query}'/data/objects/object', formats='name=id,p1,p2=p2']
----

== Section {name}

Attribute p1 has value {p1}, whereas attribute p2 has value {p2}.
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect1">
<h2 id="_section_field1">Section field1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value a, whereas attribute p2 has value b.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_field2">Section field2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value c, whereas attribute p2 has value d.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_field3">Section field3</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value e, whereas attribute p2 has value f.</p>
</div>
</div>
</div>`)
              })
              it('block ifeval test', () => {
                const doc = f(`
[xpathBlock,  ${target}example$data1.xml, ${query}'/data/objects/object','name=id,consumeronly=uniqueish']
----

=== Dependency: {name}

%ifeval::[{consumeronly} == 1]
*Only consumer is supported*
%endif::[]
%ifeval::[{consumeronly} == 2]
*Only producer is supported*
%endif::[]
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect2">
<h3 id="_dependency_field1">Dependency: field1</h3>
<div class="paragraph">
<p><strong>Only consumer is supported</strong></p>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_field2">Dependency: field2</h3>
<div class="paragraph">
<p><strong>Only consumer is supported</strong></p>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_field3">Dependency: field3</h3>
<div class="paragraph">
<p><strong>Only producer is supported</strong></p>
</div>
</div>`)
              })
              it('block nested ifeval test', () => {
                const doc = f(`
[xpathBlock,  ${target}example$data2.xml, ${query}'/mvn:project/mvn:dependencies/mvn:dependency','groupid=groupId, artifactid=artifactId, exclusions', namespaces='mvn=http://maven.apache.org/POM/4.0.0']
----

=== Dependency: {groupid}:{artifactid}

%ifeval::["{exclusions}" == ""]
The dependency {groupid}:{artifactid} has no exclusions.
%endif::[]
%ifeval::["{exclusions}" != ""]
The dependency {groupid}:{artifactid} has exclusions.
%endif::[]

[xpathBlock, example$data2.xml, '/mvn:project/mvn:dependencies/mvn:dependency[mvn:artifactId="{artifactid}"]/mvn:exclusions/mvn:exclusion','exclusion-artifactid=artifactId,exclusion-groupid=groupId', namespaces='mvn=http://maven.apache.org/POM/4.0.0']
------
==== Exclusion {exclusion-groupid}:{exclusion-artifactid}

%%ifeval::["{exclusion-groupid}"=="org.apache.activemq"]
The exclusion {exclusion-groupid}:{exclusion-artifactid} is part of activemq.
%%endif::[]
%%ifeval::["{exclusion-groupid}"!="org.apache.activemq"]
The exclusion {exclusion-groupid}:{exclusion-artifactid} is not part of activemq.
%%endif::[]

------

----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect2">
<h3 id="_dependency_org_apache_camel_quarkuscamel_quarkus_core">Dependency: org.apache.camel.quarkus:camel-quarkus-core</h3>
<div class="paragraph">
<p>The dependency org.apache.camel.quarkus:camel-quarkus-core has no exclusions.</p>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_org_apache_camel_quarkuscamel_quarkus_jms">Dependency: org.apache.camel.quarkus:camel-quarkus-jms</h3>
<div class="paragraph">
<p>The dependency org.apache.camel.quarkus:camel-quarkus-jms has exclusions.</p>
</div>
<div class="sect3">
<h4 id="_exclusion_org_apache_geronimo_specsgeronimo_jms_2_0_spec">Exclusion org.apache.geronimo.specs:geronimo-jms_2.0_spec</h4>
<div class="paragraph">
<p>The exclusion org.apache.geronimo.specs:geronimo-jms_2.0_spec is not part of activemq.</p>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_org_apache_camelcamel_activemq">Dependency: org.apache.camel:camel-activemq</h3>
<div class="paragraph">
<p>The dependency org.apache.camel:camel-activemq has exclusions.</p>
</div>
<div class="sect3">
<h4 id="_exclusion_org_apache_activemqactivemq_broker">Exclusion org.apache.activemq:activemq-broker</h4>
<div class="paragraph">
<p>The exclusion org.apache.activemq:activemq-broker is part of activemq.</p>
</div>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_org_graalvm_nativeimagesvm">Dependency: org.graalvm.nativeimage:svm</h3>
<div class="paragraph">
<p>The dependency org.graalvm.nativeimage:svm has no exclusions.</p>
</div>
</div>`)
              })
              it('block userRequire test', () => {
                const doc = f(`
[xpathBlock,  ${target}example$data2.xml, ${query}'/mvn:project/mvn:dependencies/mvn:dependency','name=artifactId,x=util(groupId),y=util.ternary(groupId==="org.apache.camel"\\,artifactId==="svm")','util=test/util/sample.js', namespaces='mvn=http://maven.apache.org/POM/4.0.0']
----

=== Dependency: {name}

{x}

{y}
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect2">
<h3 id="_dependency_camel_quarkus_core">Dependency: camel-quarkus-core</h3>
<div class="paragraph">
<p>argument is org.apache.camel.quarkus</p>
</div>
<div class="paragraph">
<p>both x and y</p>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_camel_quarkus_jms">Dependency: camel-quarkus-jms</h3>
<div class="paragraph">
<p>argument is org.apache.camel.quarkus</p>
</div>
<div class="paragraph">
<p>both x and y</p>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_camel_activemq">Dependency: camel-activemq</h3>
<div class="paragraph">
<p>argument is org.apache.camel</p>
</div>
<div class="paragraph">
<p>x only</p>
</div>
</div>
<div class="sect2">
<h3 id="_dependency_svm">Dependency: svm</h3>
<div class="paragraph">
<p>argument is org.graalvm.nativeimage</p>
</div>
<div class="paragraph">
<p>y only</p>
</div>
</div>`)
              })
            })
          })
        })
      })

      describe('xpathAttributes include processor tests', () => {
        it('object properties', () => {
          const doc = f(`include::xpath$example$data2.xml[query='/mvn:project/mvn:parent', formats='artifactId,groupId,myVersion=version', namespaces='mvn=http://maven.apache.org/POM/4.0.0']

artifactId: {artifactId}
groupId: {groupId}
version: {myVersion}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>artifactId: camel-quarkus-activemq-parent
groupId: org.apache.camel.quarkus
version: 2.0.1-SNAPSHOT</p>
</div>`)
        })

        it('simple name expressions', () => {
          const doc = f(`include::xpath$example$data2.xml[query='/mvn:project/mvn:dependencies/mvn:dependency', formats='artifactId=groupId', namespaces='mvn=http://maven.apache.org/POM/4.0.0']

camel-quarkus-core: {camel-quarkus-core}
camel-quarkus-jms: {camel-quarkus-jms}
camel-activemq: {camel-activemq}
svm: {svm}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>camel-quarkus-core: org.apache.camel.quarkus
camel-quarkus-jms: org.apache.camel.quarkus
camel-activemq: org.apache.camel
svm: org.graalvm.nativeimage</p>
</div>`)
        })

        it('complex name expressions', () => {
          const doc = f(`include::xpath$example$data2.xml[query='/mvn:project/mvn:dependencies/mvn:dependency', formats='\`\${artifactId}-groupId\`=groupId', namespaces='mvn=http://maven.apache.org/POM/4.0.0']

camel-quarkus-core: {camel-quarkus-core-groupId}
camel-quarkus-jms: {camel-quarkus-jms-groupId}
camel-activemq: {camel-activemq-groupId}
svm: {svm-groupId}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>camel-quarkus-core: org.apache.camel.quarkus
camel-quarkus-jms: org.apache.camel.quarkus
camel-activemq: org.apache.camel
svm: org.graalvm.nativeimage</p>
</div>`)
        })
        it('multiple uses', () => {
          //n.b. if you create an attribute and try to use the name of that attribute on the LHS of an assignment,
          // it will get substituted!!?!
          const doc = f(`= My Page
include::xpath$example$data2.xml[query='/mvn:project/mvn:parent', formats='parent-artifactId=artifactId,groupId,myVersion=version', namespaces='mvn=http://maven.apache.org/POM/4.0.0']
include::xpath$example$data2.xml[query='/mvn:project/mvn:dependencies/mvn:dependency', formats='artifactId=groupId', namespaces='mvn=http://maven.apache.org/POM/4.0.0']
include::xpath$example$data2.xml[query='/mvn:project/mvn:dependencies/mvn:dependency', formats='\`\${artifactId}-groupId\`=groupId', namespaces='mvn=http://maven.apache.org/POM/4.0.0']

:another-attribute3: foo


artifactId: {parent-artifactId}
groupId: {groupId}
version: {myVersion}

camel-quarkus-core: {camel-quarkus-core}
camel-quarkus-jms: {camel-quarkus-jms}
camel-activemq: {camel-activemq}
svm: {svm}

camel-quarkus-core: {camel-quarkus-core-groupId}
camel-quarkus-jms: {camel-quarkus-jms-groupId}
camel-activemq: {camel-activemq-groupId}
svm: {svm-groupId}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>artifactId: camel-quarkus-activemq-parent
groupId: org.apache.camel.quarkus
version: 2.0.1-SNAPSHOT</p>
</div>
<div class="paragraph">
<p>camel-quarkus-core: org.apache.camel.quarkus
camel-quarkus-jms: org.apache.camel.quarkus
camel-activemq: org.apache.camel
svm: org.graalvm.nativeimage</p>
</div>
<div class="paragraph">
<p>camel-quarkus-core: org.apache.camel.quarkus
camel-quarkus-jms: org.apache.camel.quarkus
camel-activemq: org.apache.camel
svm: org.graalvm.nativeimage</p>
</div>`)
        })
        it('xpathcount include processor', () => {
          const doc = f(`include::xpathcount$example$data2.xml[queries='propertiescount=/mvn:project/mvn:properties,dependencycount=/mvn:project/mvn:dependencies/mvn:dependency', namespaces='mvn=http://maven.apache.org/POM/4.0.0']

propertiescount: {propertiescount}
dependencycount: {dependencycount}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>propertiescount: 1
dependencycount: 4</p>
</div>`)
        })
      })
    })
  })
})
