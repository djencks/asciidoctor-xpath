/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
require('@asciidoctor/core')()

const { expect } = require('chai')
const EventEmitter = require('events')
const xpath = require('./../lib/xpath')
const classifyContent = require('@antora/content-classifier')

const primarySiteUrl = 'https://example.com'
const path = __dirname

describe('xpath indexpage tests', () => {
  let eventEmitter
  let log

  beforeEach(() => {
    eventEmitter = new EventEmitter()
    log = []
    xpath.prependListener = (event, code) => {
      eventEmitter.prependListener(event, code)
    }
    xpath.on = (event, code) => {
      eventEmitter.on(event, code)
    }
    xpath.getLogger = (name) => {
      return {
        trace: (msg) => {
          log.push({ level: 'trace', msg })
        },
        debug: (msg) => {
          log.push({ level: 'debug', msg })
        },
        info: (msg) => {
          log.push({ level: 'info', msg })
        },
        warn: (msg) => {
          log.push({ level: 'warn', msg })
        },
        isLevelEnabled: (level) => true,
      }
    }
  })

  it('no indexpages test', () => {
    xpath.register(xpath, { config: { logLevel: 'debug' }, playbook: {} })
    expect(eventEmitter.eventNames().length).to.equal(1)
    eventEmitter.eventNames().forEach((name) => {
      expect(eventEmitter.listenerCount(name)).to.equal(1)
    })
  })
  it('indexpages test', () => {
    xpath.register(xpath, { config: { logLevel: 'debug', indexPages: [] }, playbook: {} })
    expect(eventEmitter.eventNames().length).to.equal(2)
    const counts = { contentAggregated: 1, contentClassified: 2 }
    eventEmitter.eventNames().forEach((name) => {
      expect(eventEmitter.listenerCount(name)).to.equal(counts[name])
    })
  })

  describe('tests with content', () => {
    let contentCatalog

    beforeEach(() => {
      contentCatalog = classifyContent({ site: {} }, [], {})
      const baseFile = {
        asciidoc: { xreftext: 'doctitle' },
        origin: { site: primarySiteUrl },
        out: undefined,
        pub: {
          url: primarySiteUrl + '/c1/1.0/index.html',
          moduleRootPath: '',
        },
        src: {
          component: 'c1',
          version: '1.0',
          module: 'ROOT',
          relative: 'index.adoc',
          family: 'page',
        },
        path,
      }
      const file = contentCatalog.addFile(baseFile)
      contentCatalog.addFile({
        src: Object.assign({}, file.src, { family: 'example', relative: 'templates/template1.adoc' }),
        // eslint-disable-next-line no-template-curly-in-string
        contents: Buffer.from('= This is the template for ${groupId}:${artifactId}\n\ngroupId: ${groupId}\n\nartifactId: ${artifactId}'),
      })

      contentCatalog.addFile({
        src: Object.assign({}, file.src, { family: 'example', relative: 'json/data2.xml' }),
        // eslint-disable-next-line no-template-curly-in-string
        contents: Buffer.from(`<?xml version="1.0" encoding="UTF-8"?>
<!--

    Licensed to the Apache Software Foundation (ASF) under one or more
    contributor license agreements.  See the NOTICE file distributed with
    this work for additional information regarding copyright ownership.
    The ASF licenses this file to You under the Apache License, Version 2.0
    (the "License"); you may not use this file except in compliance with
    the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.apache.camel.quarkus</groupId>
        <artifactId>camel-quarkus-activemq-parent</artifactId>
        <version>2.0.1-SNAPSHOT</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <artifactId>camel-quarkus-activemq</artifactId>
    <name>Camel Quarkus :: ActiveMQ :: Runtime</name>

    <properties>
        <camel.quarkus.jvmSince>1.0.0</camel.quarkus.jvmSince>
        <camel.quarkus.nativeSince>1.0.0</camel.quarkus.nativeSince>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.apache.camel.quarkus</groupId>
                <artifactId>camel-quarkus-bom</artifactId>
                <version>$\{project.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.apache.camel.quarkus</groupId>
            <artifactId>camel-quarkus-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.camel.quarkus</groupId>
            <artifactId>camel-quarkus-jms</artifactId>
            <!-- ActiveMQ is bound to JMS 1.1 -->
            <exclusions>
                <exclusion>
                    <groupId>org.apache.geronimo.specs</groupId>
                    <artifactId>geronimo-jms_2.0_spec</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-activemq</artifactId>
            <!-- remove broker as it brings some classes that brak native compilation -->
            <!-- we should investigate how to substitue JMX on ActiveMQ -->
            <exclusions>
                <exclusion>
                    <groupId>org.apache.activemq</groupId>
                    <artifactId>activemq-broker</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.graalvm.nativeimage</groupId>
            <artifactId>svm</artifactId>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>io.quarkus</groupId>
                <artifactId>quarkus-bootstrap-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <annotationProcessorPaths>
                        <path>
                            <groupId>io.quarkus</groupId>
                            <artifactId>quarkus-extension-processor</artifactId>
                            <version>$\{quarkus.version}</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>full</id>
            <activation>
                <property>
                    <name>!quickly</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.camel.quarkus</groupId>
                        <artifactId>camel-quarkus-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>update-extension-doc-page</id>
                                <goals>
                                    <goal>update-extension-doc-page</goal>
                                </goals>
                                <phase>process-classes</phase>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
`
        ),
      })
    })

    // afterEach(() => {
    //   if (log.length) console.log('log', log)
    // })

    const config1 = {
      playbook: {},
      config: {
        log_level: 'debug',
        indexPages: [
          {
            query: {
              version: '1.0',
              component: 'c1',
              module: 'ROOT',
              // family: 'page',
              target: 'example$json/data2.xml',
              query: '/mvn:project/mvn:dependencies/mvn:dependency',
              namespaces: 'mvn=http://maven.apache.org/POM/4.0.0',
            },
            templateId: {
              family: 'example',
              relative: 'templates/template1.adoc',
            },
            target: {
              // eslint-disable-next-line no-template-curly-in-string
              format: '`generated/${artifactId}.adoc`',
            },
          },
        ],
      },
    }

    it('expected page count', () => {
      xpath.register(xpath, config1)
      expect(eventEmitter.eventNames().length).to.equal(2)
      eventEmitter.emit('contentClassified', { contentCatalog, siteAsciiDocConfig: {} })
      expect(contentCatalog.getFiles().length).to.equal(3 + 4)
      expect(log.pop().msg.msg).to.equal('added 4 pages')
    })

    it('basic content', () => {
      xpath.register(xpath, config1)
      expect(eventEmitter.eventNames().length).to.equal(2)
      eventEmitter.emit('contentClassified', { contentCatalog, siteAsciiDocConfig: {} })
      expect(contentCatalog.getFiles().length).to.equal(3 + 4)
      expect(log.pop().msg.msg).to.equal('added 4 pages')
      const content = contentCatalog.resolvePage('1.0@c1:ROOT:generated/camel-quarkus-core.adoc').contents.toString()
      expect(content).to.equal(`= This is the template for org.apache.camel.quarkus:camel-quarkus-core

groupId: org.apache.camel.quarkus

artifactId: camel-quarkus-core`)
    })
  })
})
